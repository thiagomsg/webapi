using System;
using Microsoft.EntityFrameworkCore;
using Pets.Domain.Entity;

namespace Pets.Data.Config
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Announcement> Announcements {get; set;}
        public DbSet<User> Users {get; set;}

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Console.WriteLine("Contexto criado");

        }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            Console.WriteLine("modelos criados");
        }
    }
}