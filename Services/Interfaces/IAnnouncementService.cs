using System.Collections.Generic;
using Pets.Domain.Entity;

namespace Pets.Services.Interfaces
{
    public interface IAnnouncementService
    {
        List<Announcement> GetAll();

        Announcement GetOne(long id);

        Announcement Save (Announcement announcement);

        void Remove(long id);
        
        Announcement Update (Announcement announcement);
    }
}