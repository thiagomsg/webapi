using System.Collections.Generic;
using Pets.Domain.Entity;

namespace Pets.Services.Interfaces
{
    public interface IUserService
    {
        List<User> GetAll();

        User GetOne(long id);
        User Login(string username, string password);
        void Remove(long id);

        User Save(User user);
    }
}