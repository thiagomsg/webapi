using System.Collections.Generic;
using System.Net.Http;
using Pets.Domain.Entity;
using Pets.Repository.Interfaces;
using Pets.Services.Interfaces;

namespace Pets.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly HttpClient client;
        private IAnnouncementRepository announcementRepository;
        
        public AnnouncementService(IAnnouncementRepository announcementRepository)
        {
            this.announcementRepository = announcementRepository;
            this.client = new HttpClient();
        }
        public List<Announcement> GetAll()
        {
           return announcementRepository.GetAll();
        }

        public Announcement GetOne(long id)
        {
            return announcementRepository.GetOne(id);
        }

        public void Remove(long id)
        {
           Announcement announcement =  GetOne(id);
           if(announcement == null)
           {
               throw new NotFoundException("Announcement not found");
           }
           announcementRepository.Remove(announcement);
        }

        public Announcement Save(Announcement announcement)
        {
            return announcementRepository.Save(announcement);
        }

        public Announcement Update(Announcement announcement)
        {
            Announcement announcementToUpdate = GetOne(announcement.Id);

            if(announcement==null)
            {
                throw new NotFoundException ("Announcement not found");
            }

            //updating fields
            announcementToUpdate.Location = announcement.Location;
            announcementToUpdate.Telephone = announcement.Telephone;
            announcementToUpdate.Title = announcement.Title;
            announcementToUpdate.PetSpecie = announcement.PetSpecie;
            announcementToUpdate.PetRace = announcement.PetRace;
            announcementToUpdate.PetAge = announcement.PetAge;

            return announcementRepository.Update(announcementToUpdate);

        }
    }
}