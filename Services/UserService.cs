
using System.Collections.Generic;
using System.Net.Http;
using Pets.Domain.Entity;
using Pets.Repository.Interfaces;
using Pets.Services.Interfaces;

namespace Pets.Services
{
    public class UserService : IUserService
    {
        private readonly HttpClient client;
        private IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
            this.client = new HttpClient();
        }

        public List<User> GetAll()
        {
            return userRepository.GetAll();
        }

        public User GetOne(long id)
        {
            return userRepository.GetOne(id);
        }

        public User Login(string username, string password)
        {
            return userRepository.Login(username, password);
        }

        public void Remove(long id)
        {
            User user = userRepository.GetOne(id);
           
            if(user == null)
            throw new NotFoundException("User Not Found");
           
            userRepository.Remove(user);

        }

        public User Save(User user)
        {
            return userRepository.Save(user);
        }
    }
}