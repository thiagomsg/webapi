using System;
using System.Collections.Generic;
using System.Linq;
using Pets.Data.Config;
using Pets.Domain.Entity;
using Pets.Repository.Interfaces;

namespace Pets.Repository
{
    public class UserRepository : IUserRepository
    {
        private DatabaseContext context;
            public UserRepository(DatabaseContext context)
        {
            this.context = context;
        } 

        public List<User> GetAll()
        {
            return context.Users.ToList();
        }

        public User GetOne(long id)
        {
            return context.Users.Where((u) => u.Id == id).SingleOrDefault();
        }

        public User Login(string username, string password)
        {
           
            User user = context.Users.Where((u)=> u.Username == username & u.Password == password).SingleOrDefault();
            return user;
        }

        public void Remove(User user)
        {
            context.Users.Remove(user);
            context.SaveChanges();
        }

        public User Save(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
            return user;
        }
    }
}