using System.Collections.Generic;
using Pets.Domain.Entity;

namespace Pets.Repository.Interfaces
{
    public interface IAnnouncementRepository
    {
        List<Announcement> GetAll();

        Announcement GetOne(long id);

        Announcement Save(Announcement announcement);

        void Remove(Announcement announcement);
        Announcement Update(Announcement announcement);
        

    }

}