using System.Collections.Generic;
using Pets.Domain.Entity;

namespace Pets.Repository.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetAll();
        User GetOne(long id);
        User Login(string username, string password);
        User Save(User user);
        void Remove(User user);

    }
}