using System.Collections.Generic;
using System.Linq;
using Pets.Data.Config;
using Pets.Domain.Entity;
using Pets.Repository.Interfaces;

namespace Pets.Repository
{
    public class AnnouncementRepository : IAnnouncementRepository
    {
        private DatabaseContext context;
        
        public AnnouncementRepository(DatabaseContext context)
        {
            this.context = context;
        } 
        public List<Announcement> GetAll()
        {
            return context.Announcements.ToList();
        }

        public Announcement GetOne(long id)
        {
            return context.Announcements.Where((a) => a.Id == id).SingleOrDefault();
        }

        public void Remove(Announcement announcement)
        {
           context.Announcements.Remove(announcement);
           context.SaveChanges();
        }

        public Announcement Save(Announcement announcement)
        {
            context.Announcements.Add(announcement);
            context.SaveChanges();
            return announcement;
        }

        public Announcement Update(Announcement announcement)
        {
            context.Announcements.Update(announcement);
            context.SaveChanges();
            return announcement;
        }
    }
}