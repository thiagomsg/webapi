using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Pets.Domain.Entity;
using Pets.Services.Interfaces;
using Newtonsoft.Json;
using Pets.Domain.Api;
using Pets.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;

namespace Pets.Controllers
{
    [ApiController]
    [Route("/api/pets")]

    public class PetsController : ControllerBase
    {
        private IAnnouncementService announcementService;
        private IUserService userService;

        public PetsController(IAnnouncementService announcementService, IUserService userService)
        {
            this.announcementService = announcementService;
            this.userService = userService;
        }

        ///<summary>
        ///Listar anúncios.
        ///</summary>
        ///<response code = "200">Lista de anúncios retornada com sucesso.</response>
        ///<response code = "500">Ocorreu um erro na listagem de anúncios.</response>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult<List<AnnouncementResponse>> GetAnnouncements()
        {
            List<AnnouncementResponse> announcementResponses = announcementService.GetAll().Select((announcement) => new AnnouncementResponse(announcement)).ToList();
            return Ok(announcementResponses);
        }

        ///<summary>
        ///Selecionar anúncio pelo id.
        ///</summary>
        ///<param name = "id">Id do anúncio.</param>
        ///<response code = "200">Anúncio retornado com sucesso.</response>
        ///<response code = "404">Anúncio não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao selecionar anúncio.</response>
        [HttpGet("{id}", Name = "GetAnnouncementById")]
        [AllowAnonymous]
        public ActionResult<AnnouncementResponse> GetAnnouncementById([FromRoute] long id)
        {
            Announcement announcement = announcementService.GetOne(id);
            if (announcement == null)
            {
                return NotFound("Announcement not found");
            }
            return Ok(new AnnouncementResponse(announcement));
        }

        ///<summary>
        ///Autenticar-se.
        ///</summary>
        ///<param name = "request"> From body request (username and password).</param>
        ///<response code = "200">A Autenticação foi realisada e o usuário autenticado retornado.</response>
        ///<response code = "404">Usuário não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao tentar obter o usuário.</response>
        [HttpPost("login/")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Login([FromBody] UserLoginRequest request)
        {
            User user = userService.Login(request.Username, request.Password);

            if (user == null)
            {
                return NotFound(new { message = "Invalid username or password." });
            }
            var token = TokenService.GenerateToken(user);

            UserResponse userResponse = new UserResponse(user);

            return new
            {
                userResponse = userResponse,
                token = token
            };
        }

        ///<summary>
        ///Criar autenticação (recurso público para facilitar desenvolvimento posteriormente pode se tornar um método restrito). 
        ///</summary>
        ///<param name = "request">From body request (username, password and role).</param>
        ///<response code = "201">Autenticação criada e retornada com sucesso.</response>
        ///<response code = "500">Ocorreu um erro na criação de autenticação.</response>
        [HttpPost("signin/")]
        [AllowAnonymous]
        public ActionResult<UserResponse> NewUser([FromBody] UserRequest request)
        {
            User newUser = new User
            {
                Username = request.Username,
                Password = request.Password,
                Role = request.Role
            };
            User user = userService.Save(newUser);
            UserResponse userResponse = new UserResponse(user);

            return CreatedAtRoute("GetUserById", new { id = userResponse.Id }, userResponse);
        }

        ///<summary>
        ///Criar anúncio.
        ///</summary>
        ///<param name = "request">From body request (title; location; telephone; pet specie, race and age).</param>
        ///<response code = "201">Anuncio criado e retornado com sucesso.</response>
        ///<response code = "500">Ocorreu um erro na criação de anúncio.</response>
        [HttpPost] // "http://localhost:5001/api/pets"
        [Authorize(Roles = "rescue org")]
        public ActionResult<AnnouncementResponse> NewAnnouncement([FromBody] AnnouncementRequest request)
        {


            Announcement newAnnouncement = new Announcement
            {
                Title = request.Title,
                Location = request.Location,
                Telephone = request.Telephone,
                PetSpecie = request.PetSpecie,
                PetRace = request.PetRace,
                PetAge = request.PetAge,
            };

            Announcement announcement = announcementService.Save(newAnnouncement);
            AnnouncementResponse announcementResponse = new AnnouncementResponse(announcement);
            return CreatedAtRoute("GetAnnouncementById", new { id = announcementResponse.Id }, announcementResponse);
        }

        ///<summary>
        ///Selecionar usuário pelo id.
        ///</summary>
        ///<param name = "id">Id do usuário.</param>
        ///<response code = "200">Usuário retornado.</response>
        ///<response code = "401">Usuário não autenticado.</response>
        ///<response code = "403">Acesso proibido.</response>
        ///<response code = "404">Usuário não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao selecionar usuário.</response>
        [HttpGet("user/{id}", Name = "GetUserById")]
        [Authorize(Roles = "rescue org")]
        public ActionResult<UserResponse> GetUserById([FromRoute] long id)
        {
            User user = userService.GetOne(id);

            if (user == null)
            {
                return NotFound("User Not Found");
            }

            return Ok(new UserResponse(user));
        }

        ///<summary>
        ///Listar usuários.
        ///</summary>
        ///<response code = "200">Lista de usuários retornada com sucesso.</response>
        ///<response code = "401">Usuário não autenticado.</response>
        ///<response code = "403">Acesso proibido.</response>
        ///<response code = "500">Ocorreu um erro na listagem de usuários.</response>
        [HttpGet("user/")]
        [Authorize(Roles = "rescue org")]
        public ActionResult<List<UserResponse>> GetUsers()
        {
            List<UserResponse> userResponses = userService.GetAll().Select((user) => new UserResponse(user)).ToList();
            return Ok(userResponses);
        }

        ///<summary>
        ///Adotar pet.
        ///</summary>
        ///<param name = "id">Id do anúncio do pet a ser adotado.</param>
        ///<response code = "200">Pet adotado e retornado com sucesso.</response>
        ///<response code = "401">Usuário não autenticado.</response>
        ///<response code = "403">Acesso proibido.</response>
        ///<response code = "404">Anúncio não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao adotar o pet.</response>
        [HttpGet("adopt/{id}")]
        [Authorize(Roles = "user")]
        public ActionResult AdoptById([FromRoute] long id)
        {
            Announcement announcement = announcementService.GetOne(id);
            if (announcement == null)
            {
                return NotFound("Announcement not found.");
            }

            string apiUrl = "https://some-random-api.ml/facts/cat";
            string petSpecie = announcement.PetSpecie;

            if (petSpecie.Equals("dog"))
            {
                apiUrl = "https://some-random-api.ml/facts/dog";
            }

            PetFact petFact = null;
            var client = new HttpClient();
            var jsonReq = client.GetAsync(apiUrl).ContinueWith((jsonResp) =>
            {
                var response = jsonResp.Result;
                var jsonString = response.Content.ReadAsStringAsync();
                jsonString.Wait();
                petFact = JsonConvert.DeserializeObject<PetFact>(jsonString.Result);

            });
            jsonReq.Wait();

            announcementService.Remove(id);
            petFact.Message = ($"Congratulations for adopting this {petSpecie}!");
            return Ok(petFact);
        }

        ///<summary>
        ///Excluir anúncio.
        ///</summary>
        ///<param name = "id">Id do anúncio a ser excluído</param>
        ///<response code = "204">Anúncio excluido com sucesso, nada para retornar.</response>
        ///<response code = "401">Usuário não autenticado.</response>
        ///<response code = "403">Acesso proibido.</response>
        ///<response code = "404">Anúncio não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao excluir o anúncio.</response>
        [HttpDelete("delete/{id}")]
        [Authorize(Roles = "rescue org")]
        public ActionResult DeleteAnnouncementById([FromRoute] long id)
        {
            Announcement announcement = announcementService.GetOne(id);
            if (announcement == null)
            {
                return NotFound("Announcement not found.");
            }
            announcementService.Remove(id);

            return NoContent();
        }

        ///<summary>
        ///Excluir usuário.
        ///</summary>
        ///<param name = "id">Id do usuário a ser excluído</param>
        ///<response code = "204">Usuário excluído com sucesso, nada para retornar.</response>
        ///<response code = "401">Usuário não autenticado.</response>
        ///<response code = "403">Acesso proibido.</response>
        ///<response code = "404">Usuário não encontrado.</response>
        ///<response code = "500">Ocorreu um erro ao excluir o usuário.</response>
        [HttpDelete("user/delete/{id}")]
        [Authorize(Roles = "rescue org")]
        public ActionResult DeleteUserById([FromRoute] long id)
        {
            User user = userService.GetOne(id);
            if (user == null)
            {
                return NotFound("User not found.");
            }
            userService.Remove(id);

            return NoContent();
        }

        ///<summary>
        ///Atualizar anúncio.
        ///</summary>
        ///<param name = "id">Id do anúncio a ser atualizado</param>
        ///<param name = "request">From Body request com todos os dados do anúncio atualizados.</param>
        ///<response code = "200">Anúncio atualizado.</response>
        ///<response code = "500">Ocorreu um erro ao atualizar o anúncio</response>
        [HttpPut("{id}")]
        [Authorize(Roles = "rescue org")]
        public ActionResult<AnnouncementResponse> UpdateAnnouncement([FromRoute] long id, [FromBody] AnnouncementRequest request)
        {
            Announcement updatedAncmt = new Announcement
            {
                Id = id,
                Location = request.Location,
                Telephone = request.Telephone,
                Title = request.Title,
                PetSpecie = request.PetSpecie,
                PetRace = request.PetRace,
                PetAge = request.PetAge
            };
            Announcement a = announcementService.Update(updatedAncmt);
            return Ok(new AnnouncementResponse(a));

        }
    }
}