using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pets.Domain.Entity
{
    [Table("announcement")]
    public class Announcement
    {
        [Column("id")]
        public long Id {get;set;}
        
        [Column("location")]
        public string Location{get;set;}
        
        [Column("telephone")]
        public string Telephone{get;set;}
        
        [Column("title")]
        public string Title{get; set;}
        
        [Column("pet_specie")]
        public string PetSpecie {get; set;}
        
        [Column("pet_race")]
        public string PetRace {get;set;}
        
        [Column("pet_age")]
        public int PetAge {get; set;}
        
        [Column ("creation")]
        public DateTime CreationDate{get;set;}

        public Announcement()
        {
            this.CreationDate = DateTime.Now;
        }
    }
}