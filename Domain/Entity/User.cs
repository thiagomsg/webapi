using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Pets.Domain.Entity
{
    [Table("pet_user")]
    public class User
    {
        [Column("id")]
        public int Id {get;set;}
        
        [Column("username")]
        public string Username {get; set;}

        [Column("password")]
        [JsonIgnore]
        public string Password{get;set;}

        [Column("role")]
        public string Role {get;set;}
    }
}