using Newtonsoft.Json;

namespace Pets.Domain.Entity
{

    public class PetFact

    {
        public string Message { get; set;}

        [JsonProperty("fact")]
        public string Curiosity { get; set; }

        public PetFact(string message)
        {
            this.Message = message;
        }
    }

}