using Newtonsoft.Json;
using Pets.Domain.Entity;

namespace Pets.Domain.Api
{
    public class UserResponse
    {
        public int Id {get;set;}
        
        public string Username {get; set;}
        [JsonIgnore]
        internal string Password{get;set;}

        public string Role {get;set;}

        public UserResponse() : base()
        {

        }   
        public UserResponse(User user) : this()
        {
            if(user!=null)
            this.Id = user.Id;
            this.Username = user.Username;
            this.Password = user.Password;
            this.Role = user.Role;
        }   
    
    }
}