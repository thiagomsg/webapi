using System;
using System.ComponentModel.DataAnnotations;

namespace Pets.Domain.Api
{
    public class AnnouncementRequest
    {
        [Required]
        public string Location { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string PetSpecie { get; set; }
        [Required]
        public string PetRace { get; set; }
        [Required]
        public int PetAge { get; set; }

    }
}