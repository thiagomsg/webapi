using System;
using Pets.Domain.Entity;

namespace Pets.Domain.Api
{
    public class AnnouncementResponse
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }

        public string Telephone { get; set; }

        public string PetSpecie { get; set; }
        public string PetRace { get; set; }
        public int PetAge { get; set; }
        public DateTime CreationDate { get; set; }
        public AnnouncementResponse() : base()
        {

        }

        public AnnouncementResponse(Announcement announcement) : this()
        {
            if (announcement != null)
            this.Id = announcement.Id;
            this.Title = announcement.Title;
            this.Location = announcement.Location;
            this.Telephone = announcement.Telephone;
            this.PetSpecie = announcement.PetSpecie;
            this.PetRace = announcement.PetRace;
            this.PetAge = announcement.PetAge;
            this.CreationDate = announcement.CreationDate;

        }

    }

}