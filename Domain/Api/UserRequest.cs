using System.ComponentModel.DataAnnotations;

namespace Pets.Domain.Api
{
    public class UserRequest
    {   
        [Required]
        public string Username { get; set; }
        
        [Required]
        public string Password { get; set; }

        [Required]
        public string Role { get; set; }
    }
}