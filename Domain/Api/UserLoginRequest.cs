using System.ComponentModel.DataAnnotations;

namespace Pets.Domain.Api
{
    public class UserLoginRequest
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}